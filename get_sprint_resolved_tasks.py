#!/usr/bin/env python3
"""
Returns resolved tickets in a project
"""

__author__ = "Damilare Adedoyin"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import configparser
import csv
import logging
import logging.handlers
import pathlib
import sys
import textwrap
import requests

USER_DIR = str(pathlib.Path.home())
_LOGGER = 1

def parse_arguments():
    """parse command line arguments into various thresholds
    :returns: dict containing validated arguments
    """
    parser = argparse.ArgumentParser(
        description=("Returns resolved tickets in a sprint's done column"),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        This can be used to pull the resolved tasks in the done column at the end of the sprint.
        '''),
    )

    parser.add_argument('query', nargs=1,
                        help='project name (slug) to query, ie: fundraising-backlog')

    parser.add_argument('-a', '--api-token', dest='api_token',
                        help='API token to use, overrides entry in the config file')

    parser.add_argument('-o', '--outfile', dest='outfile',
                        help='if specified, store data in output file')

    parser.add_argument('--conf', dest='conf',
                        help='path to configuration file (%(default)s)',
                        default=USER_DIR + "/.phab.cnf")

    parser.add_argument('--log_level',
                        help='syslog level (default=%(default)s)', metavar='LEVEL',
                        default='info', choices=['debug', 'info', 'warning', 'error', 'critical'])

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument("-v", "--verbose", "-d", "--debug",
                        dest='verbose',
                        help="verbose / debug (-v, -vv, etc)",
                        action="count", default=0)

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    return args


def setup_logging(args):
    """set up console output and syslogging scheme
    :param args: command line args from argparse
    :returns: global log handler _LOGGER
    """

    # set up main logger instance
    global _LOGGER
    _LOGGER = logging.getLogger('GlobalLogger')
    _LOGGER.setLevel(logging.DEBUG)

    # set up console handler
    console_log_level = logging.CRITICAL
    if args.verbose is True:
        console_log_level = logging.DEBUG
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_level)
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    # set up syslog handler
    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
    syslog_handler.setLevel(
        getattr(logging, args.log_level.upper(), 'info')
    )
    syslog_handler.setFormatter(
        logging.Formatter(
            "%(pathname)s[%(process)d]: %(message)s"
        )
    )

    # attach handlers to main logger instance
    _LOGGER.addHandler(console_handler)
    _LOGGER.addHandler(syslog_handler)


def parse_config(args):
    """parse the config file

    :returns: configuration details
    :rtype: dict
    """
    # print("using config file %s" % args.conf)
    config = configparser.RawConfigParser()
    # print("reading config file")
    config.read(args.conf)
    # print("read config file")
    details_dict = dict(config.items('phab'))

    return details_dict


def query_project(api_key, name):
    """query project by name or slug

    :param api_key: API key to use
    :type api_key: string
    :param name: project name to query
    :type name: string
    :returns: json of projects that match
    """
    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/project.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[query]="Fundraising Sprint Technical debt house of horrors"

    response = None
    url = "https://phabricator.wikimedia.org/api/project.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[query]': name,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching project info: %s", error)

    if response.json()['result'] is None:
        print("Failed to query projects")
        status = 1
        result = response.json()['error_info']

    return response.json()


def query_resolved_tasks_in_column(api_key, project_phid):
    """query open tasks in a column

    :param api_key: API key to use
    :type api_key: string
    :param project_phid: project PHID
    :type project_phid: string
    :returns: dict of tasks in the column and their data
    """

    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/maniphest.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[projects][0]=${PROJECT_PHID}
    # -d constraints[statuses][0]="resolved"

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[projects][0]': project_phid,
        'constraints[statuses][0]': 'resolved',
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching column info: %s", error)

    return response.json()


def main():
    """fetches arguments, read config, run check, produce report
    """
    # parse args
    args = parse_arguments()
    # setup logging
    setup_logging(args)

    config = parse_config(args)

    project = {}
    api_token = ""

    if args.api_token:
        api_token = args.api_token
    else:
        api_token = config['api_token']

    # Query for the project
    project_info = query_project(api_token, args.query)

    # Exit if we have a failure querying the projects. Other things will fail
    # after this and it is most likely due to a back api key or connection
    if project_info['result'] == None:
        print(project_info)
        exit(1)

    if len(project_info['result']['data']) > 1:
        print("More than one project matches.")
        print("Please select which project you wish:")
        for match in range(len(project_info['result']['data'])):
            possible_match = project_info['result']['data'][match]['fields']['name']
            print("  [%s] %s" % (match, possible_match))
        selection = int(input("Select one number: "))
        if selection <= len(project_info['result']['data']) - 1:
            project['phid_proj'] = project_info['result']['data'][selection]['phid']
        else:
            print("Invalid option chosen. Exiting...")
            sys.exit(1)
    else:
        project['phid_proj'] = project_info['result']['data'][0]['phid']

    resolved_tasks_info = query_resolved_tasks_in_column(api_token, project['phid_proj'])

    project['tasks'] = {}
    task_inc = 0
    for task in range(len(resolved_tasks_info['result']['data'])):
        project['tasks'][task_inc] = {
            'subject': resolved_tasks_info['result']['data'][task]['fields']['name'],
            'points_estimate': resolved_tasks_info['result']['data'][task]['fields']['points'],
            'id': resolved_tasks_info['result']['data'][task]['id'],
        }
        task_inc += 1

    if task_inc == 0:
        print("No resolved tasks. Exiting")
        sys.exit(0)

    print(f"Found {len(project['tasks'])} tasks.")

    if args.outfile:
        with open(args.outfile, 'w', newline='') as csvfile:
            outputfile = csv.writer(csvfile, delimiter=',')
            outputfile.writerow(['Phabricator ID', 'Title', 'Link'])
            for task in range(len(project['tasks'])):
                text = get_phab_text(project, task, True)
                outputfile.writerow(text)
        print("Wrote data to: %s" % args.outfile)
    else:
        print('\nSummary')
        for task in range(len(project['tasks'])):
            text = get_phab_text(project, task)
            print(text)

def get_phab_text(project, task, is_csv=False):
    est_id = project['tasks'][task]['id']
    est_title = project['tasks'][task]['subject']
    est_title = est_title.replace('"','\'')
    est_link = f"https://phabricator.wikimedia.org/T{str(est_id)}"
    if is_csv:
        return [f"T{est_id}", est_title, est_link]
    return f'- {est_title} - {est_link}'


if __name__ == "__main__":
    main()
