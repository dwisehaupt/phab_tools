#!/usr/bin/env python3
"""
Task resolver with final points option
"""

__author__ = "Dallas Wisehaupt"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import configparser
import logging
import logging.handlers
import pathlib
import sys
import textwrap
import requests

USER_DIR = str(pathlib.Path.home())
_LOGGER = 1

def parse_arguments():
    """parse command line arguments into various thresholds
    :returns: dict containing validated arguments
    """
    parser = argparse.ArgumentParser(
        description=('Task resolver assistant'),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        This can be used to resolve a task and add final points.
        '''),
    )

    parser.add_argument('task', nargs=1,
                        help='task to update, ie: T324628')

    parser.add_argument('--conf', dest='conf',
                        help='path to configuration file (%(default)s)',
                        default=USER_DIR + "/.phab.cnf")

    parser.add_argument('--points', dest='points',
                        help="final point value to set on task")

    parser.add_argument('--log_level',
                        help='syslog level (default=%(default)s)', metavar='LEVEL',
                        default='info', choices=['debug', 'info', 'warning', 'error', 'critical'])

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument("-v", "--verbose", "-d", "--debug",
                        dest='verbose',
                        help="verbose / debug (-v, -vv, etc)",
                        action="count", default=0)

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version=f"%(prog)s (version {__version__})")

    args = parser.parse_args()
    return args


def setup_logging(args):
    """set up console output and syslogging scheme
    :param args: command line args from argparse
    :returns: global log handler _LOGGER
    """

    # set up main logger instance
    global _LOGGER
    _LOGGER = logging.getLogger('GlobalLogger')
    _LOGGER.setLevel(logging.DEBUG)

    # set up console handler
    console_log_level = logging.CRITICAL
    if args.verbose is True:
        console_log_level = logging.DEBUG
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_level)
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    # set up syslog handler
    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
    syslog_handler.setLevel(
        getattr(logging, args.log_level.upper(), 'info')
    )
    syslog_handler.setFormatter(
        logging.Formatter(
            "%(pathname)s[%(process)d]: %(message)s"
        )
    )

    # attach handlers to main logger instance
    _LOGGER.addHandler(console_handler)
    _LOGGER.addHandler(syslog_handler)


def parse_config(args):
    """parse the config file

    :returns: configuration details
    :rtype: dict
    """
    # print("using config file %s" % args.conf)
    config = configparser.RawConfigParser()
    # print("reading config file")
    config.read(args.conf)
    # print("read config file")
    details_dict = dict(config.items('phab'))

    return details_dict


def close_task_with_final_points(api_key, task, points):
    """Add/edit points on a task

    :param api_key: API key to use
    :type api_key: string
    :param task: task to update
    :type task: string
    :param points: final points to apply
    :type points: int
    :returns: array of result status code and task id on success or error info on failure
    """
    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/maniphest.edit \\
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d transactions[0][type]=points \\
    # -d transactions[0][value][0]=$POINTS \\
    # -d objectIdentifier=$TASK

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.edit"

    post_data = {
        'api.token': api_key,
        'objectIdentifier': task,
        'transactions[0][type]': 'status',
        'transactions[0][value]': 'resolved',
    }

    if points is not None:
        post_data['transactions[1][type]'] = 'custom.points.final'
        post_data['transactions[1][value]'] = points

    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error updating task: %s", error)
        sys.exit(1)

    if response.json()['result'] is None:
        print("Failed to update the task")
        status = 1
        result = response.json()['error_info']
    else:
        print("Updated the task")
        status = 0
        result = response.json()['result']['object']['id']

    # return:
    #   Status: 0|1
    #   Result: On success, task id|On failure, error info

    return[status,result]


def main():
    """fetches arguments, read config, update task
    """
    # parse args
    args = parse_arguments()
    # setup logging
    setup_logging(args)
    config = parse_config(args)
    points = args.points

    #print(f"We would update the task here. task: {args.task}, final points: {points}")

    status, result = close_task_with_final_points(config['api_token'], args.task, points)

    if status == 0:
        print(f"https://phabricator.wikimedia.org/T{result}")
        sys.exit(0)
    else:
        print(f"Error: {result}")
        sys.exit(1)


if __name__ == "__main__":

    main()
