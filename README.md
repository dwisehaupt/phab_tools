# Phabricator CLI tools

This is a set of tools that are useful for interacting with phabricator. Most
require the use of a config file to perform actions. The file (~/.phab.cnf) is
an ini style file with the phabricator API key for who is going to do the
updates.

Example:
```ini
[phab]
API_TOKEN = api-token-goes-here
```

You can create/manage tokens using the following link:
  <https://phabricator.wikimedia.org/settings/user/$USERNAME/page/apitokens/>

# Tools

## dump_column_to_csv.py

Usage: `dump_column_to_csv.py phab_board`

Example: `dump_column_to_csv.py -o output.csv fundraising-backlog`

Used for pulling any tasks from a column and outputing a CSV of the TaskID,
Subject, and URL. By default it will output CSV format that could be
copied and pasted into another file or setup. Optionally, specifying `-o
filename.csv` will output the data into a csv file.

You will specify the workboard and then select the column you wish to dump.

## find_hidden_open_tasks.py

Usage: `find_hidden_open_tasks.py phab_board`

Example: `find_hidden_open_tasks.py fundraising-backlog`

Queries a phab workboard for any tasks that are still open in columns that are
hidden. This allows you to find tasks that may have been reopened after
closing but are not in general view in other columns. It will return up to 100
tasks per column.

This is used for phab task maintenance by FR-Tech.

## phab_sprint_estimation_dump.py

Usage: `phab_sprint_estimation_dump.py phab_board`

Example: `phab_sprint_estimation_dump.py fundraising-backlog`

Used for pulling any tasks from a column that have no estimated point values
assigned and dumping them to CSV format for importing into Poinz or another
tool. The script will walk you through selecting the proper board and column
you wish to dump.

Typically used on the Sprint+1 column of the Fundraising-Backlog to estimate
upcoming work on a regular basis.

## sprint_point_report.py

Usage: `sprint_point_report.py sprint_slug_name`

Example: `sprint_point_report.py keep_ya`

Takes the input of a full or partial sprint slug name (ie: sprint name with \_'s
instead of spaces). It counts the number of tasks and the sprint and the
initial and final story points for each task in the sprint.

Reports the following information:
* Sprint name
* Sprint slug
* Initial story points
* Final story points
* Total tasks in sprint

Used shortly after the sprint retro to capture the state of a sprint at
completion.

## task_apply_acl.py

Usage: `task_apply_acl.py TaskID`

Example: `task_apply_acl.py --acl fr T324628`

Secures a task by adding a project tag to a task. It also adjusts the view and
edit policies. Currently you can choose between the default of NDA or the
WMF-FR project. Tag application is additive so it will not remove other tags
on the task.

https://phabricator.wikimedia.org/tag/wmf-nda/
https://phabricator.wikimedia.org/tag/acl_wmf-fr/

## task_assign_points.py

Usage: `task_assign_points.py [--points VALUE] TaskID`

Example: `task_assign_points.py --points 1 T324628`

Assigns the *Estimated Story Points* value for a task. If no point value is
provided, it will prompt for a value to be used.

Used during regular estimation.

## task_resolve.py

Usage: `task_resolve.py [--points VALUE] TaskID`

Example: `task_resolve.py --points 1 T324628`

Moves a task into the *Resolved* state. If a point value is provided, it
assigns the *Final Story Points* value for a task.

Used during sprint review to resolve and assign final points. Also used ad hoc
when closing out tasks.

## get_sprint_resolved_tasks.py

Usage: `get_sprint_resolved_tasks.py SprintTitle [--outfile FileName.csv]`

Example: `get_sprint_resolved_tasks.py XenoRyet --outfile SprintX.csv`

Gets all resolved tasks in a Project (Sprint).

Used after sprint retro for reporting on completed tasks.