#!/usr/bin/env python3
"""
Make a task NDA or WMF-FR compliant by adding the tag and adjusting policies
"""

__author__ = "Dallas Wisehaupt"
__version__ = "0.2.0"
__license__ = "MIT"

import argparse
import configparser
import logging
import logging.handlers
import pathlib
import sys
import textwrap
import requests

USER_DIR = str(pathlib.Path.home())
_LOGGER = 1

def parse_arguments():
    """parse command line arguments into various thresholds
    :returns: dict containing validated arguments
    """
    parser = argparse.ArgumentParser(
        description=('Task security assistant'),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        This can be used to apply the NDA or acl*WMF-FR tag to a task and
        adjust the view and edit policies properly. By default it applies the
        NDA tag and policies.

        The tags are additive and will not remove or adjust existing tags.

        https://phabricator.wikimedia.org/tag/wmf-nda/
        https://phabricator.wikimedia.org/tag/acl_wmf-fr/
        '''),
    )

    parser.add_argument('task', nargs=1,
                        help='task to update, ie: T324628')

    parser.add_argument('--conf', dest='conf',
                        help='path to configuration file (%(default)s)',
                        default=USER_DIR + "/.phab.cnf")

    parser.add_argument('--log_level',
                        help='syslog level (default=%(default)s)', metavar='LEVEL',
                        default='info', choices=['debug', 'info', 'warning', 'error', 'critical'])

    parser.add_argument('--acl', '--tag', dest='acl',
                        help='tag / acl to apply (default=%(default)s)',
                        default='nda', choices=['fr', 'nda'])

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument("-v", "--verbose", "-d", "--debug",
                        dest='verbose',
                        help="verbose / debug (-v, -vv, etc)",
                        action="count", default=0)

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version=f"%(prog)s (version {__version__})")

    args = parser.parse_args()
    return args

tags = {}
# pulled from known acl*WMF-FR task and WMF-FR project
tags['fr'] = {
        'view': 'PHID-PROJ-sfhyiodzezjs4vqfk4fr',
        'edit': 'PHID-PROJ-sfhyiodzezjs4vqfk4fr',
        'tag': 'PHID-PROJ-sfhyiodzezjs4vqfk4fr',
}
# pulled from known NDA task and WMF-NDA project
tags['nda'] = {
        'view': 'PHID-PLCY-3apo2mbkx4syq2dvumty',
        'edit': 'PHID-PLCY-q33fplznxyxjfjyycma4',
        'tag': 'PHID-PROJ-ibxm3v6ithf3jpqpqhl7',
}

def setup_logging(args):
    """set up console output and syslogging scheme
    :param args: command line args from argparse
    :returns: global log handler _LOGGER
    """

    # set up main logger instance
    global _LOGGER
    _LOGGER = logging.getLogger('GlobalLogger')
    _LOGGER.setLevel(logging.DEBUG)

    # set up console handler
    console_log_level = logging.CRITICAL
    if args.verbose is True:
        console_log_level = logging.DEBUG
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_level)
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    # set up syslog handler
    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
    syslog_handler.setLevel(
        getattr(logging, args.log_level.upper(), 'info')
    )
    syslog_handler.setFormatter(
        logging.Formatter(
            "%(pathname)s[%(process)d]: %(message)s"
        )
    )

    # attach handlers to main logger instance
    _LOGGER.addHandler(console_handler)
    _LOGGER.addHandler(syslog_handler)


def parse_config(args):
    """parse the config file

    :returns: configuration details
    :rtype: dict
    """
    # print("using config file %s" % args.conf)
    config = configparser.RawConfigParser()
    # print("reading config file")
    config.read(args.conf)
    # print("read config file")
    details_dict = dict(config.items('phab'))

    return details_dict


def apply_tag_and_acl(api_key, task, project_tag, policy_edit, policy_view):
    """adjust tag and polices to make a task NDA

    :param api_key: API key to use
    :type api_key: string
    :param task: task to update
    :type task: string
    :param project_tag: project tag to apply
    :type project_tag: string
    :param policy_edit: edit policy to apply
    :type policy_edit: string
    :param policy_view: view policy to apply
    :type policy_view: string
    :returns: array of result status code and task id on success or error info on failure
    """
    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/maniphest.edit \\
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d transactions[0][type]=key \\
    # -d transactions[0][value][0]=$VALUE \\
    # -d objectIdentifier=$TASK

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.edit"

    post_data = {
        'api.token': api_key,
        'objectIdentifier': task,
        # Set view policy
        'transactions[0][type]': 'view',
        'transactions[0][value]': policy_view,
        # Set edit policy
        'transactions[1][type]': 'edit',
        'transactions[1][value]': policy_edit,
        # Add NDA tag
        'transactions[2][type]': 'projects.add',
        'transactions[2][value][0]': project_tag,
    }

    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error updating task: %s", error)
        sys.exit(1)

    if response.json()['result'] is None:
        print("Failed to update the task")
        status = 1
        result = response.json()['error_info']
    else:
        print("Updated the task")
        status = 0
        result = response.json()['result']['object']['id']

    # return:
    #   Status: 0|1
    #   Result: On success, task id|On failure, error info

    return[status,result]


def main():
    """fetches arguments, read config, update task
    """
    # parse args
    args = parse_arguments()
    # setup logging
    setup_logging(args)
    config = parse_config(args)

    acl_tag = tags[args.acl]['tag']
    acl_edit = tags[args.acl]['edit']
    acl_view = tags[args.acl]['view']

    status, result = apply_tag_and_acl(config['api_token'], args.task, acl_tag, acl_edit, acl_view)

    if status == 0:
        print(f"https://phabricator.wikimedia.org/T{result}")
        sys.exit(0)
    else:
        print(f"Error: {result}")
        sys.exit(1)


if __name__ == "__main__":

    main()
