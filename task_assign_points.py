#!/usr/bin/env python3
"""
Task point adjustment assistant
"""

__author__ = "Dallas Wisehaupt"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import configparser
import logging
import logging.handlers
import pathlib
import sys
import textwrap
import requests

USER_DIR = str(pathlib.Path.home())
_LOGGER = 1

def parse_arguments():
    """Parse command line arguments into various thresholds

    :returns: dict containing validated arguments
    """
    parser = argparse.ArgumentParser(
        description=('Task point adjustment assistant'),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        This can be used to pull add or update the Estimated Story Points for
        a task.
        '''),
    )

    parser.add_argument('task', nargs=1,
                        help='task to update, ie: T324628')

    parser.add_argument('--conf', dest='conf',
                        help='path to configuration file (%(default)s)',
                        default=USER_DIR + "/.phab.cnf")

    parser.add_argument('--points', dest='points',
                        help="point value to set on task")

    parser.add_argument('--log_level',
                        help='syslog level (default=%(default)s)', metavar='LEVEL',
                        default='info', choices=['debug', 'info', 'warning', 'error', 'critical'])

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument("-v", "--verbose", "-d", "--debug",
                        dest='verbose',
                        help="verbose / debug (-v, -vv, etc)",
                        action="count", default=0)

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version=f"%(prog)s (version {__version__})")

    args = parser.parse_args()
    return args


def setup_logging(args):
    """Set up console output and syslogging scheme

    :param args: command line args from argparse
    :returns: global log handler _LOGGER
    """

    # set up main logger instance
    global _LOGGER
    _LOGGER = logging.getLogger('GlobalLogger')
    _LOGGER.setLevel(logging.DEBUG)

    # set up console handler
    console_log_level = logging.CRITICAL
    if args.verbose is True:
        console_log_level = logging.DEBUG
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_level)
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    # set up syslog handler
    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
    syslog_handler.setLevel(
        getattr(logging, args.log_level.upper(), 'info')
    )
    syslog_handler.setFormatter(
        logging.Formatter(
            "%(pathname)s[%(process)d]: %(message)s"
        )
    )

    # attach handlers to main logger instance
    _LOGGER.addHandler(console_handler)
    _LOGGER.addHandler(syslog_handler)


def parse_config(args):
    """Parse the config file

    :returns: configuration details
    :rtype: dict
    """
    # print("using config file %s" % args.conf)
    config = configparser.RawConfigParser()
    # print("reading config file")
    config.read(args.conf)
    # print("read config file")
    details_dict = dict(config.items('phab'))

    return details_dict


def add_points_to_task(api_key, task, points):
    """Add/edit points on a task

    :param api_key: API key to use
    :type api_key: string
    :param task: task to update
    :type task: string
    :param points: points to apply
    :type points: int
    :returns: json of result
    """
    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/maniphest.edit \\
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d transactions[0][type]=points \\
    # -d transactions[0][value][0]=$POINTS \\
    # -d objectIdentifier=$TASK

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.edit"
    post_data = {
        'api.token': api_key,
        'objectIdentifier': task,
        'transactions[0][type]': 'points',
        'transactions[0][value]': points,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error updating task: %s", error)
        sys.exit(1)

    print(f"Updated the task: {task}, points: {points}")

    return response.json()


def main():
    """Fetch arguments, read config, update task
    """
    # parse args
    args = parse_arguments()
    # setup logging
    setup_logging(args)
    config = parse_config(args)
    points = args.points

    if args.points is None:
        points = int(input("Please set a point value: "))

    #print(f"We would update the task here. task: {args.task}, points: {points}")

    task_update_result = add_points_to_task(config['api_token'], args.task, points)

    if args.verbose is True:
        print(task_update_result)

    sys.exit(0)


if __name__ == "__main__":

    main()
