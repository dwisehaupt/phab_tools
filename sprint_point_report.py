#!/usr/bin/env python3
"""
Sprint point report
"""

__author__ = "Dallas Wisehaupt"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import configparser
import logging
import logging.handlers
import pathlib
import sys
import textwrap
import requests

USER_DIR = str(pathlib.Path.home())
_LOGGER = 1

def parse_arguments():
    """parse command line arguments into various thresholds
    :returns: dict containing validated arguments
    """
    parser = argparse.ArgumentParser(
        description=('Sprint point report'),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        This script will query a sprint board and report:
            * the number of tasks in the sprint
            * the sum of estimated story points
            * the sum of final story points

        This is currently used by FR-Tech for sprint reporting and management.
        '''),
    )

    parser.add_argument('query', nargs=1,
                        help='sprint name (slug) to query, ie: onion_pit')

    parser.add_argument('--conf', dest='conf',
                        help='path to configuration file (%(default)s)',
                        default=USER_DIR + "/.phab.cnf")

    parser.add_argument('--log_level',
                        help='syslog level (default=%(default)s)', metavar='LEVEL',
                        default='info', choices=['debug', 'info', 'warning', 'error', 'critical'])

    # Optional only report on the done column
    parser.add_argument("--done", "--just-done",
                        help="only report on the done column",
                        default=False, action='store_true')


    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument("-v", "--verbose", "-d", "--debug",
                        dest='verbose',
                        help="verbose / debug (-v, -vv, etc)",
                        action="count", default=0)

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    return args


def setup_logging(args):
    """set up console output and syslogging scheme
    :param args: command line args from argparse
    :returns: global log handler _LOGGER
    """

    # set up main logger instance
    global _LOGGER
    _LOGGER = logging.getLogger('GlobalLogger')
    _LOGGER.setLevel(logging.DEBUG)

    # set up console handler
    console_log_level = logging.CRITICAL
    if args.verbose is True:
        console_log_level = logging.DEBUG
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_level)
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    # set up syslog handler
    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
    syslog_handler.setLevel(
        getattr(logging, args.log_level.upper(), 'info')
    )
    syslog_handler.setFormatter(
        logging.Formatter(
            "%(pathname)s[%(process)d]: %(message)s"
        )
    )

    # attach handlers to main logger instance
    _LOGGER.addHandler(console_handler)
    _LOGGER.addHandler(syslog_handler)


def parse_config(args):
    """parse the config file

    :returns: configuration details
    :rtype: dict
    """
    # print("using config file %s" % args.conf)
    config = configparser.RawConfigParser()
    # print("reading config file")
    config.read(args.conf)
    # print("read config file")
    details_dict = dict(config.items('phab'))

    return details_dict


def query_sprint(api_key, name):
    """query sprint by name or slug

    :param api_key: API key to use
    :type api_key: string
    :param name: sprint name to query
    :type name: string
    :returns: json of projects that match
    """
    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/project.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[query]="Fundraising Sprint Technical debt house of horrors"

    response = None
    url = "https://phabricator.wikimedia.org/api/project.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[query]': name,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def query_columns_of_project(api_key, project_phid):
    """query columns of a project

    :param api_key: API key to use
    :type api_key: string
    :param project_phid: project PHID
    :type project_phid: string
    :returns: dict of columns
    """

    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/project.column.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[projects][0]=${PROJECT_PHID}

    response = None
    url = "https://phabricator.wikimedia.org/api/project.column.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[projects][0]': project_phid,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def query_tasks_of_project(api_key, project_phid):
    """query to get info on all tasks of a project
    does not include column information

    :param api_key: API key to use
    :type api_key: string
    :param project_phid: project PHID
    :type project_phid: string
    :returns: dict of tasks
    """

    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/manifest.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[projects][0]=${PROJECT_PHID}

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[projects][0]': project_phid,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def query_tasks_in_column(api_key, column_phid):
    """query sprint by name

    :param api_key: API key to use
    :type api_key: string
    :param column_phid: project PHID
    :type column_phid: string
    :returns: dict of tasks in the column and their data
    """

    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/maniphest.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[columnPHIDs][0]=${COLUMN_PHID}

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[columnPHIDs][0]': column_phid,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def main():
    """fetches arguments, read config, run check, produce report
    """
    args = parse_arguments()
    setup_logging(args)
    config = parse_config(args)

    sprint = {}

    # Query for the project
    project_info = query_sprint(config['api_token'], args.query)
    if len(project_info['result']['data']) > 1:
        print("More than one project matches. Please try a better defined query.")
        print("Possible matches (up to 100):")
        for match in range(len(project_info['result']['data'])):
            possible_match = project_info['result']['data'][match]['fields']['name']
            print("  " + possible_match)
        sys.exit(1)
    else:
        sprint['id'] = project_info['result']['data'][0]['id']
        sprint['phid_proj'] = project_info['result']['data'][0]['phid']
        sprint['name'] = project_info['result']['data'][0]['fields']['name']
        sprint['slug'] = project_info['result']['data'][0]['fields']['slug']

    # Get the column info for the project
    column_info = query_columns_of_project(config['api_token'], sprint['phid_proj'])

    sprint['columns'] = {}
    for column in range(len(column_info['result']['data'])):
        col_name = column_info['result']['data'][column]['fields']['name']
        sprint['columns'][col_name] = {
            'phid_pcol': column_info['result']['data'][column]['phid'],
            'col_name': column_info['result']['data'][column]['fields']['name'],
        }

    if args.done is True:
        # Get the task info only for the Done column
        done_phid = sprint['columns']['Done']['phid_pcol']
        task_info = query_tasks_in_column(config['api_token'], done_phid)
    else:
        # Get the task info for the project
        task_info = query_tasks_of_project(config['api_token'], sprint['phid_proj'])

    sprint['tasks'] = {}
    for task in range(len(task_info['result']['data'])):
        task_phid = task_info['result']['data'][task]['phid']
        sprint['tasks'][task_phid] = {
            'subject': task_info['result']['data'][task]['fields']['name'],
            'points_estimate': task_info['result']['data'][task]['fields']['points'],
            'points_final': task_info['result']['data'][task]['fields']['custom.points.final'],
        }

    # Get the info on each column:
    #   Story points (estimated)
    #   Final story points
    #   Ticket count

    initial_points = 0
    final_points = 0
    for task in sprint['tasks']:
        if isinstance(sprint['tasks'][task]['points_estimate'], str):
            initial_points += int(sprint['tasks'][task]['points_estimate'])
        elif isinstance(sprint['tasks'][task]['points_estimate'], int):
            initial_points += sprint['tasks'][task]['points_estimate']

        if isinstance(sprint['tasks'][task]['points_final'], int):
            final_points += sprint['tasks'][task]['points_final']

    print('Sprint name:           %s' % sprint['name'])
    print('Sprint slug:           %s' % sprint['slug'])
    print('Initial story points:  %s' % initial_points)
    print('Final story points:    %s' % final_points)
    #print('Total tasks in sprint: %s' % tasks_in_sprint)
    print('Total tasks in sprint: %s' % len(sprint['tasks'].keys()))

if __name__ == "__main__":

    main()
