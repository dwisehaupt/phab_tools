#!/usr/bin/env python3
"""
Sprint estimation assistant
"""

__author__ = "Dallas Wisehaupt"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import configparser
import logging
import logging.handlers
import pathlib
import re
import sys
import textwrap
import requests

USER_DIR = str(pathlib.Path.home())
DEFAULT_COLUMNS = ['Current', 'Sprint', 'Triage', 'Q1', 'Q2', 'Q3', 'Q4']
_LOGGER = 1

def parse_arguments():
    """parse command line arguments into various thresholds
    :returns: dict containing validated arguments
    """
    parser = argparse.ArgumentParser(
        description=('Sprint estimation assistant'),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
        This can be used to pull unestimated tasks from a column of a
        phabricator workboard. It is used in FR-Tech for pulling tasks for
        estimation. It will then output a CSV that can be imported into Poinz
        for easier estimation.
        '''),
    )

    parser.add_argument('query', nargs=1,
                        help='project name (slug) to query, ie: fundraising-backlog')

    parser.add_argument('--conf', dest='conf',
                        help='path to configuration file (%(default)s)',
                        default=USER_DIR + "/.phab.cnf")

    parser.add_argument('--column', dest='column', action='append', nargs='?',
                        help="Column(s) to search for tasks (%s)" % DEFAULT_COLUMNS)

    parser.add_argument('--log_level',
                        help='syslog level (default=%(default)s)', metavar='LEVEL',
                        default='info', choices=['debug', 'info', 'warning', 'error', 'critical'])

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument("-v", "--verbose", "-d", "--debug",
                        dest='verbose',
                        help="verbose / debug (-v, -vv, etc)",
                        action="count", default=0)

    # Specify output of "--version"
    parser.add_argument("--version", action="version",
                        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    return args


def setup_logging(args):
    """set up console output and syslogging scheme
    :param args: command line args from argparse
    :returns: global log handler _LOGGER
    """

    # set up main logger instance
    global _LOGGER
    _LOGGER = logging.getLogger('GlobalLogger')
    _LOGGER.setLevel(logging.DEBUG)

    # set up console handler
    console_log_level = logging.CRITICAL
    if args.verbose is True:
        console_log_level = logging.DEBUG
    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_log_level)
    console_handler.setFormatter(logging.Formatter("%(message)s"))

    # set up syslog handler
    syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
    syslog_handler.setLevel(
        getattr(logging, args.log_level.upper(), 'info')
    )
    syslog_handler.setFormatter(
        logging.Formatter(
            "%(pathname)s[%(process)d]: %(message)s"
        )
    )

    # attach handlers to main logger instance
    _LOGGER.addHandler(console_handler)
    _LOGGER.addHandler(syslog_handler)


def parse_config(args):
    """parse the config file

    :returns: configuration details
    :rtype: dict
    """
    # print("using config file %s" % args.conf)
    config = configparser.RawConfigParser()
    # print("reading config file")
    config.read(args.conf)
    # print("read config file")
    details_dict = dict(config.items('phab'))

    return details_dict


def query_sprint(api_key, name):
    """query sprint by name or slug

    :param api_key: API key to use
    :type api_key: string
    :param name: sprint name to query
    :type name: string
    :returns: json of projects that match
    """
    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/project.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[query]="Fundraising Sprint Technical debt house of horrors"

    response = None
    url = "https://phabricator.wikimedia.org/api/project.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[query]': name,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def query_columns_of_project(api_key, project_phid):
    """query columns of a project

    :param api_key: API key to use
    :type api_key: string
    :param project_phid: project PHID
    :type project_phid: string
    :returns: dict of columns
    """

    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/project.column.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[projects][0]=${PROJECT_PHID}

    response = None
    url = "https://phabricator.wikimedia.org/api/project.column.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[projects][0]': project_phid,
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def query_open_tasks_in_column(api_key, column_phid):
    """query sprint by name

    :param api_key: API key to use
    :type api_key: string
    :param column_phid: project PHID
    :type column_phid: string
    :returns: dict of tasks in the column and their data
    """

    # Query (via curl)
    # curl https://phabricator.wikimedia.org/api/maniphest.search \
    # -d api.token=INSERT_YOUR_API_TOKEN_HERE \
    # -d queryKey=all \
    # -d constraints[columnPHIDs][0]=${COLUMN_PHID}
    # -d constraints[statuses][0]="open"

    response = None
    url = "https://phabricator.wikimedia.org/api/maniphest.search"
    post_data = {
        'api.token': api_key,
        'queryKey': 'all',
        'constraints[columnPHIDs][0]': column_phid,
        'constraints[statuses][0]': 'open',
    }
    try:
        response = requests.post(url, data=post_data, timeout=5)
    except requests.ConnectionError as error:
        _LOGGER.critical("Connection error fetching sprint info: %s", error)

    return response.json()


def main():
    """fetches arguments, read config, run check, produce report
    """
    # parse args
    args = parse_arguments()
    # setup logging
    setup_logging(args)
    config = parse_config(args)

    if args.column is None:
        print("assigning default columns")
        args.column = DEFAULT_COLUMNS

    sprint = {}

    # Query for the project
    project_info = query_sprint(config['api_token'], args.query)
    if len(project_info['result']['data']) > 1:
        print("More than one project matches.")
        print("Please select which project you wish:")
        for match in range(len(project_info['result']['data'])):
            possible_match = project_info['result']['data'][match]['fields']['name']
            print("  [%s] %s" % (match, possible_match))
        selection = int(input("Select one number: "))
        if selection <= len(project_info['result']['data']) - 1:
            sprint['phid_proj'] = project_info['result']['data'][selection]['phid']
        else:
            print("Invalid option chosen. Exiting...")
            sys.exit(1)
    else:
        sprint['phid_proj'] = project_info['result']['data'][0]['phid']

    # Get the column info for the project
    column_info = query_columns_of_project(config['api_token'], sprint['phid_proj'])

    sprint['column'] = {}
    print('Available columns')
    for match in range(len(column_info['result']['data'])):
        col_name = column_info['result']['data'][match]['fields']['name']

        # we ignore case on the columns to make matching easier and adding
        # custom columns easier on the command line
        for entry in args.column:
            if re.search(r"^" + re.escape(entry) + r".*", col_name, re.IGNORECASE):
                print("  [%s] %s" % (match, col_name))

    col_selection = int(input('Which column would you like to dump? '))

    sprint['column'] = {
        'phid_pcol': column_info['result']['data'][col_selection]['phid'],
        #'col_name': column_info['result']['data'][col_selection]['fields']['name'],
    }

    # Get the task info only for tasks with no points
    column_phid = sprint['column']['phid_pcol']
    task_info = query_open_tasks_in_column(config['api_token'], column_phid)

    sprint['tasks'] = {}
    task_inc = 0
    for task in range(len(task_info['result']['data'])):
        if task_info['result']['data'][task]['fields']['subtype'] == 'spike':
            # don't want to collect spike tasks
            continue
        if task_info['result']['data'][task]['fields']['subtype'] == 'bug':
            # don't want to collect bug tasks
            continue
        if task_info['result']['data'][task]['fields']['points'] is None:
            sprint['tasks'][task_inc] = {
                'subject': task_info['result']['data'][task]['fields']['name'],
                'points_estimate': task_info['result']['data'][task]['fields']['points'],
                'id': task_info['result']['data'][task]['id'],
            }
            task_inc += 1

    if task_inc == 0:
        print("No unsetimated tasks. Exiting")
        sys.exit(0)

    print("Found %s tasks." % task_inc)
    tasks_to_estimate = int(
        input("How many tasks would you like to estimate (default 10): ") or "10")

    print()

    print('key,title,description')
    for task in range(0, min(len(sprint['tasks']), tasks_to_estimate)):
        est_id = sprint['tasks'][task]['id']
        est_title = sprint['tasks'][task]['subject']
        est_title = est_title.replace('"','\'')
        est_url = 'https://phabricator.wikimedia.org/T' + str(est_id)
        print("T%s,\"%s\",%s" % (est_id, est_title, est_url))


if __name__ == "__main__":

    main()
